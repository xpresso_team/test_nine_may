"""
Manages the HTTP request/response
"""

__all__ = ['HTTPHandler']
__author__ = 'Naveen Sinha'

import os
import warnings
import requests

from xpresso.ai.core.commons.utils.constants import CONFIG_SSL, \
    CONFIG_SSL_VERIFY
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.network.http.http_request import HTTPRequest
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.network.http.http_response import HTTPResponse
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    HTTPRequestFailedException
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    HTTPInvalidRequestException

# We are doing this to suppress all warning from the python clients
warnings.simplefilter("ignore")  # Change the filter in this process
os.environ["PYTHONWARNINGS"] = "ignore"

class HTTPHandler:
    """
    Manages the HTTP request and response for external queries.
    """

    def __init__(self):
        self.logger = XprLogger()
        self.config = XprConfigParser()
        self.empty_response = HTTPResponse(500, {}, {}, {})

    def send_request(self, request: HTTPRequest) -> HTTPResponse:
        """
        Sends the HTTP request and gets the response

        Args:
            request(HTTPRequest): Object of HTTP Request which contains
                                  necessary request data

        Returns:
            HTTPResponse: response object
        """

        try:
            self.logger.debug(f"Sending HTTP Request: {request}")

            # This will be set to true once we have valid certificates
            additional_option = {
                CONFIG_SSL_VERIFY: False
            }
            try:
                additional_option[CONFIG_SSL_VERIFY] = \
                    self.config[CONFIG_SSL][CONFIG_SSL_VERIFY]
            except (KeyError, TypeError):
                self.logger.warning("Additional configuration processing "
                                    "failed. Ignoring...")

            if request.method == HTTPMethod.GET:
                response = requests.get(request.url,
                                        json=request.data,
                                        headers=request.headers,
                                        auth=request.auth,
                                        **additional_option)
            elif request.method == HTTPMethod.POST:
                response = requests.post(request.url,
                                         json=request.data,
                                         headers=request.headers,
                                         auth=request.auth,
                                         **additional_option)
            elif request.method == HTTPMethod.PUT:
                response = requests.put(request.url,
                                        json=request.data,
                                        headers=request.headers,
                                        auth=request.auth,
                                        **additional_option)
            elif request.method == HTTPMethod.DELETE:
                response = requests.delete(request.url,
                                           json=request.data,
                                           headers=request.headers,
                                           auth=request.auth,
                                           **additional_option)
            elif request.method == HTTPMethod.HEAD:
                response = requests.head(request.url,
                                         json=request.data,
                                         headers=request.headers,
                                         auth=request.auth,
                                         **additional_option)
            elif request.method == HTTPMethod.OPTIONS:
                response = requests.options(request.url,
                                            json=request.data,
                                            headers=request.headers,
                                            auth=request.auth,
                                            **additional_option)
            else:
                raise HTTPInvalidRequestException("Invalid HTTP Method")
            parsed_response = self.parse_response(response)
            self.logger.debug(f"Received HTTP Request: {parsed_response}")
        except (requests.HTTPError, requests.ConnectionError,
                requests.ConnectTimeout, requests.exceptions.SSLError) as e:
            self.logger.error(e)
            raise HTTPRequestFailedException("Request Failed")
        except (requests.exceptions.InvalidHeader,
                requests.exceptions.InvalidSchema,
                requests.exceptions.InvalidURL, ValueError) as e:
            self.logger.error(e)
            raise HTTPInvalidRequestException("Invalid Request Object")
        return parsed_response

    def parse_response(self, response: requests.Response) -> HTTPResponse:
        """ Convert requests.Response into HTTP Response object"""
        # If the response is an instance of requests.Response class
        # then return an empty response
        if not isinstance(response, requests.Response):
            return self.empty_response
        return HTTPResponse(response_code=response.status_code,
                            response_data=response.text,
                            response_headers=response.headers,
                            response_links=response.links)


__all__ = ["AbstractPipelineComponent"]
__author__ = "KK"

import sys
import os
import shutil
import time
import threading
import json

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    PipelineComponentException, InvalidMetrics, ReportKPIsFailed
from xpresso.ai.core.commons.utils.datetime_utils import DatetimeUtils
from xpresso.ai.core.commons.utils.generic_utils import str2bool
from xpresso.ai.core.commons.utils.run_metrics_utils import MetricTypes, \
    check_and_drop_invalid_kpis, check_and_drop_invalid_timeseries
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.data.pipeline.interface_pipeline_component import \
    InterfacePipelineComponent
from xpresso.ai.core.data.pipeline.RunFieldValue import \
    RunFieldValue
from xpresso.ai.core.data.pipeline.pipeline_component_controller import \
    PipelineController
from xpresso.ai.core.commons.utils.constants import ENABLE_LOCAL_EXECUTION, \
    PARAMS_FILENAME_ENV, PARAMS_COMMIT_ID_ENV, XPRESSO_MOUNT_PATH, \
    DEFAULT_MOUNT_PATH_RUNS, DEFAULT_MOUNT_PATH_OLD, XPRESSO_PIPELINE_MOUNT_PATH, \
    METRICS_VALUE, TIME
from xpresso.ai.core.data.pipeline.report_status import StatusReporter
from xpresso.ai.client.controller_client import ControllerClient
from xpresso.ai.core.data.versioning.controller_factory \
    import VersionControllerFactory


class AbstractPipelineComponent(InterfacePipelineComponent):

    def __init__(self, name=None, run_name=None, params_filename=None,
                 params_commit_id=None):

        if name is None:
            raise PipelineComponentException()

        super().__init__()

        self.logger = XprLogger()

        # set run name
        self.xpresso_run_name = run_name

        # set component name
        self.name = os.environ.get("COMPONENT_NAME", name)

        # set run_status
        self.run_status = RunFieldValue.RUN_STATUS_IDLE.value

        # set component_status
        self.component_status = RunFieldValue.RUN_STATUS_IDLE.value

        # state of component (to be saved on pause, and loaded on restart)
        self.state = None

        # status of component (to be reported on periodic basis - consists of status dict and metrics dict)
        self.status = None

        # output of component (to be stored on disk on completion)
        self.output = None

        # final results of component (to be stored in database on completion)
        self.results = None

        self.run_status_thread = None

        self.should_thread_continue = True

        self.status_reporter = None

        if str2bool(os.environ.get(ENABLE_LOCAL_EXECUTION, "False")):
            self.local_execution = True

        else:
            self.controller = PipelineController()
            self.local_execution = False

        self.OUTPUT_DIR = PipelineController.OUTPUT_DIR

        self.parameters_filename = params_filename if params_filename \
            else os.environ.get(PARAMS_FILENAME_ENV, None)

        self.parameters_commit_id = params_commit_id if params_commit_id \
            else os.environ.get(PARAMS_COMMIT_ID_ENV, None)

        self.run_parameters = self.fetch_run_parameters()

        self.create_symlink()

    def create_symlink(self):
        """ creates symlink for /expt mount path """
        # checking for old mount path for backward compatibility
        pipeline_mount_path = DEFAULT_MOUNT_PATH_OLD if os.path.isdir(
            DEFAULT_MOUNT_PATH_OLD) else os.environ.get(
            XPRESSO_MOUNT_PATH,
            os.environ.get(XPRESSO_PIPELINE_MOUNT_PATH, None))

        if os.path.exists(DEFAULT_MOUNT_PATH_RUNS):
            try:
                shutil.rmtree(DEFAULT_MOUNT_PATH_RUNS)
            except Exception:
                self.logger.warning(f"Remove failed for {DEFAULT_MOUNT_PATH_RUNS}")        

        if pipeline_mount_path and not os.path.exists(DEFAULT_MOUNT_PATH_RUNS):
            os.symlink(f'{pipeline_mount_path}/{self.xpresso_run_name}',
                       DEFAULT_MOUNT_PATH_RUNS)

    def fetch_run_parameters(self):
        """ fetches run parameters from file or commit id"""

        if self.local_execution and not self.xpresso_run_name and \
                self.parameters_filename:  # for local testing
            try:
                with open(self.parameters_filename) as f:
                    file_parameters = json.load(f)
                return file_parameters if file_parameters else {}
            except FileNotFoundError:
                self.logger.debug(f'Parameters file not found at local '
                                  f'path {self.parameters_filename}')
                return {}
        if not self.parameters_filename and not self.parameters_commit_id:
            return {}
        run_parameters = self.controller.load_parameters(
            xpresso_run_name=self.xpresso_run_name,
            parameters_filename=self.parameters_filename,
            commit_id=self.parameters_commit_id)
        return run_parameters

    def report_status(self, status):
        """ Report status to the controller """
        self.status_reporter.report_pipeline_status(status=status)

    def report_kpi_metrics(self, metrics: dict):
        """
            Report KPIs for visualization
            Allowed input;
                - list of float/int (1D arrays for now)
                - float/int
            Note: Metric reported again will overwrite
                  previous value
        Args:
            metrics(dict): metrics as key-value pairs
        Examples:
            {
                "data_row": 154100,
                "loss": 0.231,
                "accuracy": 0.6431,
                "loss_curve": [
                    0.941, 0.642, 0.431, 0.231
                ]
            }
        """
        print("Reporting KPIs")
        # Type/Format checks for metrics dictionary
        try:
            metrics = check_and_drop_invalid_kpis(metrics)
        except Exception as e:
            raise InvalidMetrics(str(e))

        if not metrics:
            return

        # Wrapping metrics under component name
        metrics = {self.name: metrics}

        if self.local_execution:
            status = True
            print(metrics)
        else:
            status = self.controller.send_metrics(
                self.xpresso_run_name, metrics, MetricTypes.KPIs.value)

        if not status:
            raise ReportKPIsFailed("Report KPIs failed")
        elif not status[0]:
            raise ReportKPIsFailed(status[1])
        print("Report KPIs complete")

    def report_timeseries_metrics(self, metrics: dict):
        """
            Report Time-series for visualization
            Allowed input;
                - float/int
        Args:
            metrics(dict): metrics as key-value pairs
        Examples:
            {
                "data_row": 154100,
                "loss": 0.231,
                "accuracy": 0.6431,
            }
            reporting these metrics again will record
            the value against new timestamps
        """
        print("Reporting Time-series")
        # Type/Format checks for metrics dictionary
        try:
            metrics = check_and_drop_invalid_timeseries(metrics)
        except Exception as e:
            raise InvalidMetrics(str(e))

        if not metrics:
            return

        modified_metrics = dict()
        for key, value in metrics.items():
            modified_metrics.update({key: list()})
            modified_metrics[key].append(
                {METRICS_VALUE: value,
                 TIME: DatetimeUtils.get_current_formatted_time()})

        # Wrapping metrics under component name
        metrics = {self.name: modified_metrics}

        if self.local_execution:
            status = True
            print(metrics)
        else:
            status = self.controller.send_metrics(
                self.xpresso_run_name, metrics, MetricTypes.TIME_SERIES.value)

        if not status:
            raise ReportKPIsFailed("Report Time-series failed")
        elif not status[0]:
            raise ReportKPIsFailed(status[1])

        print("Report Time-series complete")

    def start(self, xpresso_run_name):

        """
        start the experiment corresponding to the xpresso_run_name
        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        print("Parent component starting", flush=True)
        self.xpresso_run_name = xpresso_run_name

        if self.status_reporter is None:
            self.status_reporter = StatusReporter(component_name=self.name,
                                                  xpresso_run_name=self.xpresso_run_name)

        if self.local_execution:
            return
        # check status immediately and take action if required
        try:
            self.get_run_status()
        except Exception as e:
            print("Failed to check the status: {}".format(str(e)), flush=True)

        # start thread to check run status
        self.run_status_thread = threading.Thread(target=self.check_run_status)
        self.run_status_thread.start()
        self.should_thread_continue = True
        try:
            self.controller.pipeline_component_started(self.xpresso_run_name,
                                                       self.name)
        except Exception as e:
            print("Failed to send start status: {}".format(str(e)), flush=True)

    def terminate(self):
        print("Parent component terminating", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        try:
            self.controller.pipeline_component_terminated(self.xpresso_run_name,
                                                          self.name)
        except Exception as e:
            print("Failed to send terminate status: {}".format(str(e)), flush=True)
        finally:
            sys.exit(0)

    def pause(self, push_exp=False):

        print("Parent component saving state and exiting", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        try:
            self.controller.pipeline_component_paused(self.xpresso_run_name,
                                                      self.name,
                                                      self.state,
                                                      push_exp=push_exp)
        except Exception as e:
            print("Failed to send pause status: {}".format(str(e)), flush=True)
        finally:
            sys.exit(0)

    def restart(self):
        print("Parent component restarting", flush=True)
        if self.local_execution:
            return

        state = None
        try:
            state = self.controller.pipeline_component_restarted(
                self.xpresso_run_name,
                self.name)
        except Exception as e:
            print("Failed to send restart status: {}".format(str(e)), flush=True)

        if state is not None:
            self.state = state
            try:
                self.controller.update_field(self.xpresso_run_name,
                                             field_value=RunFieldValue.RUN_STATUS_RUNNING.value)
            except Exception as e:
                print("Failed to update RUNNING status: {}".format(str(e)), flush=True)

        else:
            print(
                "This component already completed execution. Some other "
                "component needs to restart. Terminating", flush=True)
            self.terminate()

    def completed(self, push_exp=True, success=True):
        print("Parent component completed", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        try:
            self.controller.pipeline_component_completed(
                self.xpresso_run_name, self.name, self.results,
                push_exp=push_exp, success=success,
                run_parameters=self.run_parameters)
        except Exception as e:
            print("Failed to send completed status: {}".format(str(e)), flush=True)
        finally:
            if not success:
                sys.exit(1)
            sys.exit(0)

    def exit_thread(self):
        """
        called when execution of the component is completed
        """
        print("Parent component exiting", flush=True)
        if self.local_execution:
            return

        self.should_thread_continue = False
        sys.exit(0)

    def check_run_status(self):
        while self.should_thread_continue:
            try:
                self.get_run_status()
            except Exception as e:
                print("Failed to check the status: {}".format(str(e)), flush=True)
            time.sleep(5)
        print("Stopping the check run status thread", flush=True)

    def get_run_status(self):
        self.run_status, self.component_status = \
            self.controller.get_pipeline_run_status(
                self.xpresso_run_name, self.name)
        self.logger.info(f"RUN STATUS: {self.run_status}\n "
                         f"COMPONENT STATUS: {self.component_status}")
        if self.run_status == RunFieldValue.RUN_STATUS_IDLE.value:
            pass
        elif self.run_status == RunFieldValue.RUN_STATUS_RUNNING.value:
            pass
        elif self.run_status == RunFieldValue.RUN_STATUS_TERMINATE.value \
                or self.run_status == 'TERMINATE':
            # Adding the 'or' case for backward compatibility
            self.logger.info("Terminating component")
            self.terminate()
        elif self.run_status == RunFieldValue.RUN_STATUS_PAUSED.value:
            self.logger.info("Pausing component")
            self.pause()
        elif self.run_status == RunFieldValue.RUN_STATUS_RESTART.value:
            self.logger.info("Restarting component")
            self.restart()
        elif self.run_status == RunFieldValue.RUN_STATUS_COMPLETED.value:
            self.logger.info("Completing component")
            self.controller.pipeline_component_completed(
                self.xpresso_run_name, self.name, self.results, push_exp=False,
                success=True, run_parameters=self.run_parameters)
            self.exit_thread()
        else:
            self.logger.info("No action required...continuing")

        if self.component_status == RunFieldValue.RUN_STATUS_COMPLETED.value:
            self.logger.info("Completing component")
            self.exit_thread()

    def get_version_controller(self, username, password):
        """
        Creates a version controller object for performing data versioning
        operations
        Args:
            username: xpresso uid with access to the corresponding repository
            password: password associated with the xpresso uid

        Returns: versioning controller object

        """
        ControllerClient().login(username=username, password=password)
        self.logger.info("Login successful. "
                         "Proceeding to fetch a version controller...")
        controller_factory = VersionControllerFactory()
        version_controller = controller_factory.get_version_controller()
        self.logger.info("Successfully fetched a version controller."
                         "Returning...")
        return version_controller

